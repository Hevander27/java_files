package classroom;


public class Flight {

	public String InitialDes, FinalDes;
	public int availability;
	public int id;
	public double price;
	
	public Flight(String InitialDes, String FinalDes, int availability, double price) {
		
		this.InitialDes = InitialDes;
		this.FinalDes = FinalDes;
		this.availability = availability;
		this.price = price;
		
	}
	
	public String toString() {
		
		String output = InitialDes + ", "+ FinalDes + ", Open Seats: "+ availability 
						+ ", Flight ID: " + id + ", Price: $" + price;
		
		
		return output ; 
	}
	
	
	
	
	

}
