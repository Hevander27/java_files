package classroom;



public class SearchCriteria {

	String source, destination;
	
	
	public SearchCriteria (String initialDes, String finalDes) {
		this.source = initialDes;
		this.destination = finalDes;
		
	}	
	boolean flightMatch(Flight flight) {
		
		if(destination != null) {
			
			if(!destination.equalsIgnoreCase(flight.FinalDes )) {
				return false;
			}
					
		}
		
		if(source != null) {
			
			if(!source.equalsIgnoreCase(flight.InitialDes )) {
				return false;
			}
					
		}		
			
		
		return true;
		
	}
}
