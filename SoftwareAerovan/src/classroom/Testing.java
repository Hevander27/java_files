package classroom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;



public class Testing {

	FlightStorage base = new FlightStorage();
	PurchaseStorage Pur_base = new PurchaseStorage();
	public Testing() {
		
		base.listFlight(new Flight("Atlanta", "Chicago",16, 132));
		
		base.listFlight(new Flight("Denver", "New York",0, 434));
		
		base.listFlight(new Flight("Atlanta", "Los Angeles",29, 455));
		
		base.listFlight(new Flight("Atlanta", "Chicago",20, 312));
		
		base.listFlight(new Flight("Atlanta", "Austin",20, 413));
		
		base.listFlight(new Flight("Atlanta", "Chicago",43, 732));
		
		base.listFlight(new Flight("Seatle", "Chicago",2, 256));

	}
	
	
	public void SearchDialog() {
		
		Scanner scan = new Scanner(System.in);
	
		System.out.println("Input city of departure");
		String source = scan.nextLine();
		
		System.out.println("Input city of arrival");
		String sink = scan.nextLine();
		
		if(source.equals(""))
			source = null;
		
		if(sink.equals(""))
			sink = null;
		
		
		SearchCriteria search = new SearchCriteria(source, sink);
		
		ArrayList<Flight> Results = base.searchFlight(search);
				
		
		
		if(Results.size()==0)
			System.out.println("No available listings");
		else {
			System.out.printf("%d flights found:\n", Results.size());
			for (Flight flight : Results) {
				System.out.printf("\t%s\n",flight.toString());
				
			}
		}
	}


	public void ListDialog() {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Input City of Flight Departure");
		String source = scan.nextLine();
		
		System.out.println("Input City of Flight Destination ");
		String sink = scan.nextLine();
		
		System.out.println("Input available seating in Flight");
		int availability  = scan.nextInt();
		
		System.out.println("Input price for this flight");
		double price  = scan.nextDouble();
		
		Flight flight = new Flight(source, sink,availability, price);
		
		base.listFlight(flight);
		
		System.out.println("Flight number: " + flight.id + " Succesfully Listed ");
		
	}


	public void BuyDialog() {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Input Flight ID");
		int FlightID = scan.nextInt(); 
		scan.nextLine();
		
		Flight flight = base.findFlight(FlightID);
		
		if(flight.availability == 0) {
			System.out.println("No seats available");
			
			return;
		}
		
		System.out.println("Flight: "  + flight + "\n");
		
		System.out.println("Enter \"yes\" to accept the flight, Enter \"no\" to cancel ");
		
		boolean wasAccepted;
		while(true) {
			
			String response = scan.nextLine();
			
			if(response.equalsIgnoreCase("yes")) {
				wasAccepted = true;
				break;
			}
			else if(response.equalsIgnoreCase("no")) {
				wasAccepted = false;
				break;
			}
			else
				System.out.println("Invalid response, Please enter yes or no.");	
		}
		
		
		if(wasAccepted == true) {
			
			System.out.println("Please enter your name");
			String name = scan.nextLine();
			
			Purchase purchase = new Purchase(name,flight);
			
			Pur_base.addPurchase(purchase);
			
			System.out.println(purchase.toString());
		}

	}
	
	
	
}
