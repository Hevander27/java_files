package classroom;

import java.util.ArrayList;



public class FlightStorage {

	private ArrayList<Flight> flights = new ArrayList();
	
	ArrayList<Flight> searchFlight(SearchCriteria search) {
		
		ArrayList<Flight> matching = new ArrayList();
		
		for (Flight here : flights) {
			
			if(search.flightMatch(here)) {
				
				matching.add(here);
			}
		}
		

		
		
		return matching;
	}
	
	int next_flight_id = 0;
	
	public void listFlight(Flight flight) {
		
		flight.id = next_flight_id++;
		flights.add(flight);
		
	}
	
	public Flight findFlight(int id) {
		
		for(Flight current: flights) {
			if(current.id ==id)
				return current;
		}
		return null;
		
	}
	
	
	
}
