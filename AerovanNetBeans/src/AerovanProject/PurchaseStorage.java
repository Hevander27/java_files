package AerovanProject;

import java.util.ArrayList;

public class PurchaseStorage  {

	
	ArrayList<Purchase> purchases = new ArrayList();
	
	public Purchase lookUp(int Order_ID) {
		
		for(Purchase current: purchases) {
			if(current.getOrder_ID() ==Order_ID)
				return current;
		}
		return null;
	}
	
	int next_Order_ID = 100;
	public void addPurchase(Purchase purchase) {
		
		purchase.setOrder_ID(next_Order_ID++);
		purchases.add(purchase);
		
		purchase.getFlight().availability--;
		
		
	}
}
