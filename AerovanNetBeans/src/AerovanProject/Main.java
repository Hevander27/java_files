package AerovanProject;

import java.util.Scanner;

public class Main {

	public static void main(String args[]) {
		
		Testing tester = new Testing();
		Scanner scan = new Scanner(System.in);
		while(true) {
			
		
			
			System.out.println("\nWould you like to: \n\t \"search\" (as Customer)? \n\t \"list\" (as Airline)? \n\t \"Purchase\" (as Customer)?");
			String response = scan.nextLine();
			
			if(response.equalsIgnoreCase("search"))
				tester.SearchDialog();
			else if(response.equalsIgnoreCase("list"))
				tester.ListDialog();
			else if (response.equalsIgnoreCase("Purchase"))
				tester.BuyDialog();
			else 
				System.out.println("Invalid Command ");
			
			
		}
	}
}
