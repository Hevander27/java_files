package AerovanProject;

public class Purchase {

	public String Cus_Name;
	public Flight flight;
	public int Order_ID;
	
	public Purchase(String Cus_Name, Flight flight) {
		
		this.Cus_Name = Cus_Name;
		this.setFlight(flight);
		
	}
	
	public String toString() {
		
		String receipt = "\tReceipt:\n\n"+"\t"+ Cus_Name+ "\n\t"+ 
						 getFlight()+ "\n\t" + "Order ID: "+ getOrder_ID() ;
		
		
		return receipt ; 
	}

	public int getOrder_ID() {
		return Order_ID;
	}

	public void setOrder_ID(int order_ID) {
		Order_ID = order_ID;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}
	
}
